package com.example.example1.objects
import com.example.example1.models.User



object App {

    private var _user: User = User()

    var user: User
        get() = _user
        set(value) {
            _user = value
        }

}