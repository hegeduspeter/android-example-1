package com.example.example1.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.example1.R
import com.example.example1.objects.App
import com.example.example1.objects.AppPreferences
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user_details.*

class UserDataInputActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_data_input)
    }

    fun actionSave(view: View) {
        App.user.city = textCity.text.toString()
        AppPreferences.saveString("user", Gson().toJson(App.user))
        startActivity(Intent(this, UserDetailsActivity::class.java))
    }
}
