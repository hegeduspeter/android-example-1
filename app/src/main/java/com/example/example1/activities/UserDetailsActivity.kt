package com.example.example1.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.example1.R
import com.example.example1.objects.App
import com.example.example1.objects.AppPreferences
import kotlinx.android.synthetic.main.activity_user_details.*

class UserDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        textName.text = "Name: ${App.user.name}"
        textCity.text = "City: ${App.user.city}"
        textPhoneNumber.text = "Phone Number: ${App.user.phoneNumber}"
        textEmail.text = "Email: ${App.user.email}"
        textBirthYear.text = "Birth Year: ${App.user.birthYear}"
    }

    fun deleteUser(view: View){
        AppPreferences.saveString("user", "")
        startActivity(Intent(this, MainActivity::class.java))
    }
}
