package com.example.example1.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View

import com.example.example1.R
import com.example.example1.models.User
import com.example.example1.objects.App
import com.example.example1.objects.AppPreferences
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppPreferences.init(this)

        val userJson = AppPreferences.getString("user")

        if (userJson.isEmpty()) {
            App.user = User()
        } else {
            App.user = Gson().fromJson(userJson, User::class.java)
            if (App.user.email.isEmpty()) {
                startActivity(Intent(this, UserDataInputActivity::class.java))
            } else {
                startActivity(Intent(this, UserDetailsActivity::class.java))
            }
        }

        // TODO: rossz listener
        textName.setOnClickListener {
            btnNext.isEnabled = !textName.text.toString().isEmpty()
            // TODO: toast
        }
    }

    fun actionNext(view: View) {
        App.user.name = textName.text.toString()
        AppPreferences.saveString("user", Gson().toJson(App.user))
        startActivity(Intent(this, UserDataInputActivity::class.java))
    }

}

/*
1. Egy alkalmazás amiben egy személy adatait tároljuk el a SharedPreferences segitségével.
Az app a következőképpen müködjön: Ha nincs benne eltárolva egy user sem, akkor egy 3 screenes
 alkalmazás legyen. Első screenen leriva hogy ez az app eltárolja az ő adatait, majd a nevét
 bekérjük.  A view-t tetszés szerint készítsük el, tetszés szerinti layouttal .A mező amibe
 bekérjük a user nevét, csak 1 sort fogadjon el, és max 30 karakteres lehet (ha ezt túl
 lépjük toast üzenetben irjuk ki). a második screenen bekérjükl a user további adatait:
 születési év, város, telefonszám, email cim.

a 3. screenen megjelenitjük a user adatait. Ha a telefonszámra nyomunk , hivja fel a számot
 (a dialer-be fogja dobni elbvileg , de néhány telefonnál egyből inditja a hivást), emailra
 kattintva pedig nyiljon meg egy email app,  a tárgy automatikusan legyen kitöltve: Test app 1.

következő megnyitásra ha már csak a 3ik screen jöjjön be , mivel van elmentett user. Legyen
egy gombunk amivel törölni tudjuk. Miután töröltük dialogba értesitsük ausert hogy törölvge
lettekl az adfatok, majd oké gombra nyomva a dialopg bezárodik majd az első screennek kell
megjelennie, ahol feltudunk venni egy új usert.

Egyéb: email formátumot ellenörizzük, név megadása esetén minden szó a mezőben nagybetüvel
kezdüdjön a megjelenitett screnen (3. oldalon) erre egy metódus legyen. pl:
bevitt név : géza géza -> Géza Géza.
 */
