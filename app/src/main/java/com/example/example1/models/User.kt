package com.example.example1.models

data class User(var name: String = "",
                var email: String= "",
                var phoneNumber: String= "",
                var birthYear: Int= 1900,
                var city: String= "")